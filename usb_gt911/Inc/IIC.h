#ifndef __IIC_H__
#define __IIC_H__
#include "main.h"

#define SCLPORT IIC_SCL_GPIO_Port
#define SDAPORT IIC_SDA_GPIO_Port

#define SCLPin  IIC_SCL_Pin
#define SDAPin  IIC_SDA_Pin

#define SCL_HIGH()  SCLPORT->BSRR = SCLPin
#define SCL_LOW()   SCLPORT->BRR = SCLPin


#define SDA_HIGH()  SDAPORT->BSRR =SDAPin
#define SDA_LOW()   SDAPORT->BRR =SDAPin
#define READBit()   SDAPORT->IDR & SDAPin

void soft_i2c_start(void);
void soft_i2c_stop(void);

uint8_t soft_i2c_wait_ack(void);
void soft_i2c_ack(void);
void soft_i2c_nack(void);

void soft_i2c_send_byte(uint8_t);
uint8_t soft_i2c_read_byte(void);
uint8_t IIC_Read_Byte(unsigned char ack);
uint8_t iic_found();
#endif