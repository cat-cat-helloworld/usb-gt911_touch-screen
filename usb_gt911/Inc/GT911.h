#ifndef __GT911_H__
#define __GT911_H__
#include "stdint.h"
#include "gpio.h"
#include "main.h"


#define GT911_INT_0   IIC_INT_GPIO_Port->BRR = (uint32_t)IIC_INT_Pin//HAL_GPIO_WritePin(IIC_INT_GPIO_Port,IIC_INT_Pin,0)
#define GT911_INT_1   IIC_INT_GPIO_Port->BSRR = (uint32_t)IIC_INT_Pin //HAL_GPIO_WritePin(IIC_INT_GPIO_Port,IIC_INT_Pin,1)
 
#define GT911_RST_1  IIC_RST_GPIO_Port->BSRR = (uint32_t)IIC_RST_Pin //HAL_GPIO_WritePin(IIC_RST_GPIO_Port,IIC_RST_Pin,1)
#define GT911_RST_0  IIC_RST_GPIO_Port->BRR = (uint32_t)IIC_RST_Pin //HAL_GPIO_WritePin(IIC_RST_GPIO_Port,IIC_RST_Pin,0)

void GT911_Reset_Sequence(uint8_t ucAddr);
int  GT911_Read(uint16_t reg,uint8_t  *data);

void GT911_InitHard(void);
//int GT911_Write();
//void GT911_Read();
#endif