/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */



/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
#define u8  unsigned char
#define u16  unsigned short
#define u32  unsigned int
#define byte unsigned char	
/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define IIC_RST_Pin 				GPIO_PIN_5
#define IIC_RST_GPIO_Port 	GPIOA
#define IIC_SDA_Pin 				GPIO_PIN_6
#define IIC_SDA_GPIO_Port 	GPIOA
#define IIC_SCL_Pin 				GPIO_PIN_7
#define IIC_SCL_GPIO_Port 	GPIOA
#define IIC_INT_Pin 				GPIO_PIN_1
#define IIC_INT_GPIO_Port 	GPIOB
#define IIC_INT_EXTI_IRQn 	EXTI0_1_IRQn
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */
//void SysTick_Init(uint32_t a);
void delay_ms(uint16_t nCount);
void delay_us(uint32_t nCount);
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
