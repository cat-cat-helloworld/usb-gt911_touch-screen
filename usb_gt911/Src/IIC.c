#include "IIC.h"



void I2C_Delay(void)
{
  
	  delay_us(1);
}

void I2C_ACK_Delay(void)
{
    delay_ms(10);
	
}

void soft_i2c_start(void)
{

    SCL_HIGH();
    SDA_HIGH();
    I2C_Delay();
    I2C_Delay();
	
    SDA_LOW();
    I2C_Delay();
    I2C_Delay();
	
    SCL_LOW();
    I2C_Delay();
   
}


void soft_i2c_stop(void)
{

    //SCL_LOW();
    SDA_LOW();
    I2C_Delay();
    //I2C_Delay();

    SCL_HIGH();
    I2C_Delay();
    I2C_Delay();
	
    SDA_HIGH();
    I2C_Delay();
    I2C_Delay();
}

uint8_t soft_i2c_wait_ack(void)
{

    uint8_t isACK=1;

    SDA_HIGH();
    I2C_Delay();
	
    SCL_HIGH();
    I2C_Delay();

		
     u8 AckTime=0;
		 while(READBit())
    {
        AckTime++;
        I2C_ACK_Delay();
        if(AckTime > 5)
        {
            isACK=0;//未响应  
            break;
        }
    }
		
		
    I2C_Delay();
    SCL_LOW();
    I2C_Delay();

    return isACK;
}


void soft_i2c_ack(void)
{
// SCL_LOW();
    SDA_LOW();
    I2C_Delay();
	
    SCL_HIGH();
    I2C_Delay();
    I2C_Delay();
	
    SCL_LOW();
    I2C_Delay();
    //I2C_Delay();
    SDA_HIGH();
	  //I2C_Delay();
}


void soft_i2c_nack(void)
{
    SCL_LOW();
    SDA_HIGH();


    SCL_HIGH();
    I2C_Delay();
    I2C_Delay();
	
	
    SCL_LOW();
    I2C_Delay();
    I2C_Delay();

}


void soft_i2c_send_byte(uint8_t data)
{
    uint8_t i;

    for(i=0; i<8; i++)
    {
        if((data&0x80)!=0)
				{
            SDA_HIGH();
				}
        else
				{
            SDA_LOW();
				}
        data <<=1;
        I2C_Delay();
        SCL_HIGH();
				
        I2C_Delay();
        I2C_Delay();

        SCL_LOW();
        I2C_Delay();
    }
    SDA_HIGH();

}


uint8_t soft_i2c_read_byte(void)
{
    uint8_t i,read_data;
    read_data=0;
    for(i=0; i<8; i++)
    {
        I2C_Delay();
        SCL_HIGH();
        I2C_Delay();
			  read_data<<=1;
        if(READBit())
				{
            read_data++;
				}
        I2C_Delay();
        SCL_LOW();
        I2C_Delay();
    }
    return read_data;
}

//读1个字节，ack=1时，发送ACK，ack=0，发送nACK   
u8 IIC_Read_Byte(unsigned char ack)
{
	
	  uint8_t i,read_data;
    read_data=0;
    for(i=0; i<8; i++)
    {
        I2C_Delay();
        SCL_HIGH();
        I2C_Delay();
			  read_data<<=1;
        if(READBit())
				{
            read_data++;
				}
        I2C_Delay();
        SCL_LOW();
        I2C_Delay();
    }
	
    if (!ack)
        soft_i2c_nack();//发送nACK
    else
        soft_i2c_ack(); //发送ACK 		
		
    return read_data;
}



#include "stdio.h"
uint8_t iic_found()
{
	//uint8_t ret=0;
	  uint8_t data=0;
		for(int i=0 ; i<0xff; i++, data++){
		HAL_GPIO_WritePin(IIC_SDA_GPIO_Port, IIC_RST_Pin, 0);	
		delay_ms(10);
		HAL_GPIO_WritePin(IIC_SDA_GPIO_Port, IIC_RST_Pin, 1);	
		delay_ms(10);		
			
		soft_i2c_start();
		soft_i2c_send_byte(data);
			
		if(soft_i2c_wait_ack())
		{
			printf("found !");
		  break;
		}
		
		soft_i2c_nack();
	}
	
	soft_i2c_nack();
	return data;
}